//
//  AlertsViewControllerTests.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import XCTest

class AlertsViewControllerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        XCUIApplication().launch()
    }
    
    func testLoadingIndicatorShowsWhenButtonIsTapped() {
        XCUIApplication().buttons["Button"].tap()
        
        let loadingIndicators = XCUIApplication().activityIndicators
        XCTAssertEqual(loadingIndicators.count, 1)
    }
    
    func testAlertsDoUpdateWhenButtonIsTapped() {
        let alert = XCUIApplication().staticTexts["An alert"]
        let exists = NSPredicate(format: "exists == true")
        expectationForPredicate(exists, evaluatedWithObject: alert, handler: nil)
        
        XCUIApplication().buttons["Button"].tap()
        waitForExpectationsWithTimeout(5.0, handler: nil)
        
        XCTAssert(alert.exists)
    }
    
}