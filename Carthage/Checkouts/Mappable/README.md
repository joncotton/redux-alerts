# Mappable
Helper library to simplify mapping key value data sources to classes/structs in Swift.

## Examples

```Swift
struct Article: Mappable {
  private(set) var title = ""
  private(set) var bodyText = ""
  private(set) var link: NSURL?

  mutating func mapFromDataSource(dataSource: MappingDataSource) throws {
    try title    <- dataSource["title"]
    try bodyText <- dataSource["body"]
    _= try? link <- (dataSource["link"], Transformers.URL)
  }
}

guard let rawData = NSData(contentsOfURL: URL)
  else { return ... }

guard let someJsonData = try? NSJSONSerialization.JSONObjectWithData(rawData, options: .MutableContainers) as? [String : AnyObject]
  else { return ... }
// Given that someJsonData = ["title": "An Article", "body": "Some text...", "link": "https://www.sky.com"]

var article = Article()
article <- someJsonData

// article is now:
Article (
  title: "An Article"
  bodyText: "Some text..."
  link: NSURL(absoluteString: "https://www.sky.com")
)
```

If you prefer, you can pass in the dataSource at init time, when used in this way the initialiser is failable
```Swift
//...

guard let article = Article(mappingDataSource: someJsonData)
  else { //... }

// article is now:
Article (
  title: "An Article"
  bodyText: "Some text..."
  link: NSURL(absoluteString: "https://www.sky.com")
)
```

## How to install

...

## TODO

- Add documentation about how to install
- Add documentation to list new types/operators
- Add tests for the built in Transformers

## Contributions

Contributions are welcomed, please make all of your changes in a branch and raise a PR
