//
//  MappableTests.swift
//  MappableTests
//
//  Created by Cotton, Jonathan (Mobile Developer) on 23/12/2015.
//  Copyright © 2015 Sky. All rights reserved.
//

import XCTest
@testable import Mappable

class MappableTests: XCTestCase {
    
    private struct MockMappable: Mappable {
        private(set) var aString = ""
        private(set) var anInt = 0
        private(set) var anArray = ["a", "b", "c"]
        
        mutating func mapFromDataSource(dataSource: MappingDataSource) throws {
            try aString <- dataSource["aString"]
            try anInt   <- dataSource["anInt"]
            try anArray <- dataSource["anArray"]
        }
    }
    
    /*
        Happy paths (a little contrived as we're testing that Swift generics work correctly, but tests all the basic types map correctly)
    */
    func testStringDoesMap() {
        let testValue = "testValue"

        var aString = ""
        _ = try? aString <- testValue
        assert(aString == testValue, "\(aString) did not match expected value of \(testValue)")
    }
    
    func testIntDoesMap() {
        let testValue = 134

        var anInt = 0
        _ = try? anInt <- testValue
        assert(anInt == testValue, "\(anInt) did not match expected value of \(testValue)")
    }
    
    func testFloatDoesMap() {
        let testValue = Float(45.1)

        var aFloat = Float(0)
        _ = try? aFloat <- testValue
        assert(aFloat == testValue, "\(aFloat) did not match expected value of \(testValue)")
    }
    
    func testDoubleDoesMap() {
        let testValue = 599.23

        var aDouble = Double(0)
        _ = try? aDouble <- testValue
        assert(aDouble == testValue, "\(aDouble) did not match expected value of \(testValue)")
    }
    
    func testArrayDoesMap() {
        let testValue = ["a", "b", "c", "d", "e", "f"]

        var anArray = [""]
        _ = try? anArray <- testValue
        assert(anArray == testValue, "\(anArray) did not match expected value of \(testValue)")
    }
    
    func testDictionaryDoesMap() {
        let testValue = ["a": 1, "b": 2, "c": 3]

        var aDictionary = ["":0]
        _ = try? aDictionary <- testValue
        assert(aDictionary == testValue, "\(aDictionary) did not match expected value of \(testValue)")
    }
    
    func testOptionalWithSomeValueDoesMap() {
        let testValue: String? = "HELLO"
        
        var aString = "hello"
        _ = try? aString <- testValue
        assert(aString == testValue, "\(aString) did not match expected value of \(testValue)")
    }
    
    func testValueDoesMapToOptional() {
        let testValue = "testValue"
        
        var aString: String? = nil
        _ = try? aString <- testValue
        assert(aString == testValue, "\(aString) did not match expected value of \(testValue)")
    }
    
    func testValueDoesMapToImplicitlyUnwrappedOptional() {
        let testValue = "testValue"
        
        var aString: String! = ""
        _ = try? aString <- testValue
        assert(aString == testValue, "\(aString) did not match expected value of \(testValue)")
    }
    
    /*
        aggreagte mapping operations
    */
    func testMappableDoesMap() {
        let testData: MappingDataSource = [
            "aString": "Hello",
            "anInt"  : 999,
            "anArray": ["1", "2", "3"]
        ]
        
        var aMappable = MockMappable()
        
        do {
            try aMappable <- testData
        } catch {
            XCTFail("Unexpected error when attempting to map to aMappable")
        }
        
        assert(aMappable.aString == "Hello", "\(aMappable.aString) did not match expected value of Hello")
        assert(aMappable.anInt == 999, "\(aMappable.anInt) did not match expected value of 999")
        assert(aMappable.anArray == ["1", "2", "3"], "\(aMappable.anArray) did not match expected value of ['1', '2', '3']")
    }
    
    func testArrayOfMappablesDoesMap() {
        let testData: [MappingDataSource] = [
            [
                "aString": "Hello",
                "anInt"  : 999,
                "anArray": ["1", "2", "3"]
            ],
            [
                "aString": "Goodbye",
                "anInt"  : 101,
                "anArray": ["x", "y", "z"]
            ]
        ]
        
        var mappables = [MockMappable(), MockMappable()]
        
        do {
            try mappables <- testData
        } catch {
            XCTFail("Unexpected error when attempting to map to mappables")
        }
        
        assert(mappables[0].aString == "Hello", "\(mappables[0].aString) did not match expected value of Hello")
        assert(mappables[0].anInt == 999, "\(mappables[0].anInt) did not match expected value of 999")
        assert(mappables[0].anArray == ["1", "2", "3"], "\(mappables[0].anArray) did not match expected value of ['1', '2', '3']")
        
        assert(mappables[1].aString == "Goodbye", "\(mappables[1].aString) did not match expected value of Goodbye")
        assert(mappables[1].anInt == 101, "\(mappables[1].anInt) did not match expected value of 101")
        assert(mappables[1].anArray == ["x", "y", "z"], "\(mappables[1].anArray) did not match expected value of ['x', 'y', 'z']")
    }
    
    func testOptionalArrayOfMappablesDoesMap() {
        let testData: [MappingDataSource] = [
            [
                "aString": "Hello",
                "anInt"  : 999,
                "anArray": ["1", "2", "3"]
            ],
            [
                "aString": "Goodbye",
                "anInt"  : 101,
                "anArray": ["x", "y", "z"]
            ]
        ]
        
        var mappables: [MockMappable]?
        
        do {
            try mappables <- testData
        } catch {
            XCTFail("Unexpected error when attempting to map to mappables")
        }
        
        assert(mappables![0].aString == "Hello", "\(mappables![0].aString) did not match expected value of Hello")
        assert(mappables![0].anInt == 999, "\(mappables![0].anInt) did not match expected value of 999")
        assert(mappables![0].anArray == ["1", "2", "3"], "\(mappables![0].anArray) did not match expected value of ['1', '2', '3']")
        
        assert(mappables![1].aString == "Goodbye", "\(mappables![1].aString) did not match expected value of Goodbye")
        assert(mappables![1].anInt == 101, "\(mappables![1].anInt) did not match expected value of 101")
        assert(mappables![1].anArray == ["x", "y", "z"], "\(mappables![1].anArray) did not match expected value of ['x', 'y', 'z']")
    }
    
    /*
        Exceptions
    */
    func testValueWithMismatchedTypeDoesNotMap() {
        let testValue = 23
    
        var aString = "hello"
        _ = try? aString <- testValue
        assert(aString == "hello", "Unexpected mutation of aString")
    }
    
    func testOptionalWithNilValueDoesNotMap() {
        let testValue: String? = nil
        
        var aString = "hello"
        _ = try? aString <- testValue
        assert(aString == "hello", "Unexpected mutation of aString")
    }
    
    func testMappableThrowsOnFailedMap() {
        var didThrow = false
        
        let testData: MappingDataSource = [
            "aString" : Float(32.45),
            "anInt"   : 999,
            "anArray" : ["1", "2", "3"]
        ]
        
        var aMappable = MockMappable()

        do {
            try aMappable <- testData
        } catch {
            didThrow = true
        }
        
        assert(didThrow, "Expected mapping operation to throw error due to misnatched types")
    }
    
    /*
        Init
    */
    func testMappableCanBeInstantiatedFromDataSource() {
        let testData = [
            "aString": "Goodbye",
            "anInt"  : 101,
            "anArray": ["x", "y", "z"]
        ]
        
        let aMappable = MockMappable(mappingDataSource: testData)
        
        XCTAssertNotNil(aMappable)
    }
    
    func testMappableInitialiserDoesFailWithBadData() {
        let testData: MappingDataSource = [
            "aString" : Float(32.45),
            "anInt"   : 999,
            "anArray" : ["1", "2", "3"]
        ]
        
        let aMappable = MockMappable(mappingDataSource: testData)
        
        XCTAssertNil(aMappable)
    }
    
}
