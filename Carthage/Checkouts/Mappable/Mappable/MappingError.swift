//
//  MappingError.swift
//  Mappable
//
//  Created by Cotton, Jonathan (Mobile Developer) on 23/12/2015.
//  Copyright © 2015 Sky. All rights reserved.
//

import Foundation

public struct MappingError : ErrorType {}