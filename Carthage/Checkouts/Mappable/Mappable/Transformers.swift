//
//  Transformers.swift
//  Mappable
//
//  Created by Cotton, Jonathan (Mobile Developer) on 23/12/2015.
//  Copyright © 2015 Sky. All rights reserved.
//

import Foundation

public struct Transformers {
    private static let dateFormatter = NSDateFormatter()
    
    public static let URL = { (originalValue: String) -> NSURL? in
        return NSURL(string:originalValue)
    }
    
    public static let Date = { (originalValue: String) -> NSDate? in
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter.dateFromString(originalValue)
    }
}