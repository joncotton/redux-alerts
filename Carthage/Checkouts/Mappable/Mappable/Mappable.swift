//
//  Mappable.swift
//  Mappable
//
//  Created by Cotton, Jonathan (Mobile Developer) on 23/12/2015.
//  Copyright © 2015 Sky. All rights reserved.
//

import Foundation

public typealias MappingDataSource = [String : AnyObject]

public protocol Mappable {
    // mappable types must provide a way to be created without any dependencies
    init()
    
    // map and populate porperties from the supplied data provider
    mutating func mapFromDataSource(dataSource: MappingDataSource) throws
}

public extension Mappable {
    init?(mappingDataSource: MappingDataSource) {
        if let mappable = Self.createFromDataSource(mappingDataSource) {
            self = mappable
        } else {
            return nil
        }
    }
    
    private static func createFromDataSource(dataSource: MappingDataSource) -> Self? {
        var mappableSelf = Self()
        
        do {
            try mappableSelf <- dataSource
        } catch {
            return nil
        }
        
        return mappableSelf
    }
}
