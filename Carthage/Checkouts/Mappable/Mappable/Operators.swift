//
//  Operators.swift
//  Mappable
//
//  Created by Cotton, Jonathan (Mobile Developer) on 23/12/2015.
//  Copyright © 2015 Sky. All rights reserved.
//

import Foundation

infix operator <- {}
public func <- <T,U>(inout destination: T, source: U?) throws {
    if let sourceSameType = source as? T {
        destination = sourceSameType
    } else {
        throw MappingError()
    }
}

public func <- <T,U>(inout destination: T!, source: U?) throws {
    if let sourceSameType = source as? T {
        destination = sourceSameType
    } else {
        throw MappingError()
    }
}

public func <- <T,U>(inout destination: T?, source: U?) throws {
    if let sourceSameType = source as? T {
        destination = sourceSameType
    } else {
        throw MappingError()
    }
}

// using a transformer to change the value before assigning
public func <- <T,U,V>(inout destination: T, source: (originalValue: U?, transformer: (originalValue: V) -> (T?))) throws {
    if let
        originalValueCorrectType = source.originalValue as? V,
        transformedSource = source.transformer(originalValue: originalValueCorrectType)
    {
        destination = transformedSource
    } else {
        throw MappingError()
    }
}

public func <- <T,U,V>(inout destination: T!, source: (originalValue: U?, transformer: (originalValue: V) -> (T?))) throws {
    if let
        originalValueCorrectType = source.originalValue as? V,
        transformedSource = source.transformer(originalValue: originalValueCorrectType)
    {
        destination = transformedSource
    } else {
        throw MappingError()
    }
}

public func <- <T,U,V>(inout destination: T?, source: (originalValue: U?, transformer: (originalValue: V) -> (T?))) throws {
    if let
        originalValueCorrectType = source.originalValue as? V,
        transformedSource = source.transformer(originalValue: originalValueCorrectType)
    {
        destination = transformedSource
    } else {
        throw MappingError()
    }
}

// allows mapping directly to a mappble type
public func <- <T where T:Mappable>(inout destination: T, source: MappingDataSource?) throws {
    if let dataSource = source {
        try destination.mapFromDataSource(dataSource)
    } else {
        throw MappingError()
    }
}

public func <- <T where T:Mappable>(inout destination: T?, source: MappingDataSource?) throws {
    if destination == nil {
        destination = T()
    }
    
    if let dataSource = source {
        try destination!.mapFromDataSource(dataSource)
    } else {
        throw MappingError()
    }
}

// allows mapping a collection of mappables directly to an array of data sources
public func <- <T where T:Mappable>(inout destination: [T], source: [MappingDataSource]?) throws {
    if let dataSources = source {
        var mappableCollection = [T]()
        
        for dataSource in dataSources {
            if let mappable = T(mappingDataSource: dataSource) {
                mappableCollection.append(mappable)
            }
        }
        
        destination = mappableCollection
    } else {
        throw MappingError()
    }
}

public func <- <T where T:Mappable>(inout destination: [T]?, source: [MappingDataSource]?) throws {
    if let dataSources = source {
        var mappableCollection = [T]()
        
        for dataSource in dataSources {
            if let mappable = T(mappingDataSource: dataSource) {
                mappableCollection.append(mappable)
            }
        }
        
        destination = mappableCollection
    } else {
        throw MappingError()
    }
}