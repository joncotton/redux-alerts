Pod::Spec.new do |s|
  s.name     = 'Mappable'
  s.version  = '1.0.0'
  s.summary  = "Helper library to simplify mapping key value data sources to classes/structs in Swift."
  s.authors  = { 'Jon Cotton' => 'jonathan.cotton@sky.uk' }
  s.source   = { :git => 'https://github.com/sky-uk/mappable', :branch => 'master' }
  s.source_files = 'Mappable'
end
