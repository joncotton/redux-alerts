//
//  RemoteJSONDataProvider.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Alamofire

// TODO: No injected network lib dependency here, wouldn't be like this in prod
struct RemoteJSONDataProvider: DataProvider {
    typealias DataType = [String : AnyObject]
    
    let url: NSURL
    
    init(url: NSURL) {
        self.url = url
    }
    
    func loadData(completion: ([String : AnyObject]?) -> ()) {
        Alamofire.request(.GET, url).responseJSON { response in
            guard let JSON = response.result.value as? [String : AnyObject]
            else {
                Log("Failed to load remote JSON data from \(self.url.absoluteString)")
                completion(nil)
                return
            }
            
            completion(JSON)
        }
    }
}