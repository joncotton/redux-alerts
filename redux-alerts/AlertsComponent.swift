//
//  AlertsComponent.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 09/04/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

struct AlertsProperties: ComponentProperties {
    let alerts: [InAppAlert]
    let alertsAreVisible: Bool
    let loadingIndicatorIsVisible: Bool
    
    init(alerts: [InAppAlert], alertsAreVisible: Bool, loadingIndicatorIsVisible: Bool) {
        self.alerts = alerts
        self.alertsAreVisible = alertsAreVisible
        self.loadingIndicatorIsVisible = loadingIndicatorIsVisible
    }
}

@objc
class AlertsComponent: NSObject, Component {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var alertsStackView: UIStackView!
    
    private(set) var currentProperties: AlertsProperties?
    
    func render(properties: AlertsProperties) {
        if currentProperties?.loadingIndicatorIsVisible != properties.loadingIndicatorIsVisible {
            if properties.loadingIndicatorIsVisible {
                loadingIndicator.hidden = false
                loadingIndicator.startAnimating()
            } else {
                loadingIndicator.hidden = true
            }
        }
        
        if currentProperties?.alertsAreVisible != properties.alertsAreVisible {
            alertsStackView.hidden = !properties.alertsAreVisible
        }
        
        for (index, alert) in properties.alerts.enumerate() {
            var alertView: AlertView
            
            if alertsStackView.arrangedSubviews.count > index {
                guard let view = alertsStackView.arrangedSubviews[index] as? AlertView
                    else { continue }
                
                alertView = view
            } else {
                alertView = AlertView()
                alertsStackView.addArrangedSubview(alertView)
            }
            
            alertView.titleLabel.text = alert.title
        }
        
        for (index, view) in alertsStackView.arrangedSubviews.enumerate() {
            if index >= properties.alerts.count {
                alertsStackView.removeArrangedSubview(view)
                view.removeFromSuperview()
            }
        }
    }
}