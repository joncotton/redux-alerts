//
//  Redux.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Waterloo

protocol Dispatcher {
    func dispatch(action: Action)
    func dispatch(actionProducing: ActionProducing)
    func dispatch(actionProducingAction: ActionProducingAction)
}

final class Store: Dispatcher {
    static let sharedInstance = Store()

    private let dispatchQueue = SerialQueue(label: "redux-store-dispatch-q")
    private var state = AppState()
    
    var currentState: AppState {
        var state: AppState!
        dispatchQueue.sync {
            state = self.state
        }
        return state
    }
    
    private let stateObserversLock = NSLock()
    private var stateObservers: [StateObserver] = []
    
    func dispatch(action: Action) {
        Log("Dispatching Action: \(action)")
        
        dispatchQueue.async {
            let newState = action.execute(self.state)
            self.state = newState
        
            for observer in self.stateObservers {
                observer.stateDidUpdate(newState)
            }
        }
    }
    
    func dispatch(actionProducing: ActionProducing) {
        actionProducing.produceAction(state) { [weak self] action in
            guard let action = action
                else { return }
            
            self?.dispatch(action)
        }
    }
    
    func dispatch(actionProducingAction: ActionProducingAction) {
        dispatch(actionProducingAction as ActionProducing)
        dispatch(actionProducingAction as Action)
    }
    
    func addStateObserver<ObserverType: StateObserver>(observer: ObserverType) {
        stateObserversLock.lock()
        stateObservers.append(observer)
        stateObserversLock.unlock()
        
        observer.stateDidUpdate(state)
    }

    func removeStateObserver<ObserverType: StateObserver>(observer: ObserverType) {
        if let index = stateObservers.indexOf({ return $0 === observer }) {
            stateObserversLock.lock()
            stateObservers.removeAtIndex(index)
            stateObserversLock.unlock()
        }
    }
}

// MARK:- State Observer

protocol StateObserver: class {
    func stateDidUpdate(state: AppState)
}

// MARK:- Actions

protocol Action {
    func execute(state: AppState) -> AppState
}

protocol ActionProducing {
    func produceAction(state: AppState, completion: (Action?) -> ())
}

protocol ActionProducingAction: ActionProducing, Action {}

// MARK:- Store Consumer

protocol StoreConsumer {
    var store: Store {get}
}

extension StoreConsumer {
    var store: Store {
        return Store.sharedInstance
    }
}