//
//  EndPoints.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct EndPoints: Mappable {
    
    // TODO: This isn't actually required for NSURL as it's an immutable type, but this is here to demonstrate
    // how you could implement copy on write when holding refs to mutable reference types
    private var _serviceApp = NSURL(string: "http://localhost:3000")!
    var serviceApp: NSURL {
        get {
            return _serviceApp
        }
        set {
            guard let newURL = newValue.copy() as? NSURL
                else { return }
            
            _serviceApp = newURL
        }
    }
    
    mutating func mapFromDataSource(dataSource: MappingDataSource) throws {
        try serviceApp <- (dataSource["serviceApp"], Transformers.URL)
    }
}