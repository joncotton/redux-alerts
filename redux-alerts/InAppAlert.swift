//
//  InAppAlert.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 15/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct InAppAlert: Mappable {
    private(set) var title: String
    private(set) var description: String
    private(set) var imageName: String
    private(set) var actionURL: NSURL
    
    init() {
        title = ""
        description = ""
        imageName = ""
        actionURL = NSURL()
    }
    
    init(title: String, description: String, imageName: String, actionURL: NSURL) {
        self.title = title
        self.description = description
        self.imageName = imageName
        self.actionURL = actionURL
    }
    
    mutating func mapFromDataSource(dataSource: MappingDataSource) throws {
        try title <- dataSource["title"]
        try description <- dataSource["description"]
        try imageName <- dataSource["image-name"]
        try actionURL <- (dataSource["action-url"], Transformers.URL)
    }
}