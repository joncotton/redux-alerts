//
//  NibView.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 15/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class NibView: UIView {    
    lazy var nibView: UIView = self.loadViewFromNib("\(self.dynamicType)")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        addSubviewAndPinToEdges(nibView)
    }
    
    private func loadViewFromNib(nibName: String) -> UIView {
        let possibleViews = NSBundle.mainBundle().loadNibNamed(nibName, owner: self, options: nil)
        return possibleViews.first! as! UIView
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSizeMake(UIViewNoIntrinsicMetric, nibView.bounds.size.height)
    }
}