//
//  UpdateAppConfig.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct UpdateAppConfigAction: Action {
    typealias GenericStateType = AppState
    let config: AppConfig
    
    init(config: AppConfig) {
        self.config = config
    }
    
    func execute(state: AppState) -> AppState {
        var mutableState = state
        mutableState.config.appConfig = config
        return mutableState
    }
}