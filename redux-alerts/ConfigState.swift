//
//  File.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct ConfigState {
    var updateStatus: ConfigUpdateStatus = .initialState
    var appConfig = AppConfig()
}