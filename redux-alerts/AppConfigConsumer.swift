//
//  AppConfigConsumer.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 17/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

protocol AppConfigConsumer: StoreConsumer {
    var appConfig: AppConfig {get}
}

extension AppConfigConsumer {
    var appConfig: AppConfig {
        return store.currentState.config.appConfig
    }
}