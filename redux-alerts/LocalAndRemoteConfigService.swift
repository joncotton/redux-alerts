//
//  LocalAndRemoteConfigService.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct LocalAndRemoteConfigService: ConfigService, StoreConsumer {
    let localService: ConfigService
    let remoteService: ConfigService
    
    init(localService: ConfigService, remoteService: ConfigService) {
        self.localService = localService
        self.remoteService = remoteService
    }
    
    func latestAppConfig(completion: (AppConfig?) -> ()) {
        remoteService.latestAppConfig { config in
            guard let config = config
                else { return }
            
            let action = UpdateAppConfigAction(config: config)
            self.store.dispatch(action)
        }
        
        localService.latestAppConfig { config in
            guard let config = config
                else { return }
            
            let action = UpdateAppConfigAction(config: config)
            self.store.dispatch(action)
        }
        
    }
}