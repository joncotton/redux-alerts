//
//  LocalJSONDataProvider.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct LocalJSONDataProvider: DataProvider {
    typealias DataType = [String : AnyObject]
    
    let filename: String
    
    init(filename: String) {
        self.filename = filename
    }
    
    func loadData(completion: ([String : AnyObject]?) -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let bundle = NSBundle.mainBundle()

            guard let fullPath = bundle.pathForResource(self.filename, ofType: "json")
            else {
                Log("Local JSON load failed, failed to find \(self.filename).json")
                completion(nil)
                return
            }
            
            guard let jsonData = NSData(contentsOfFile: fullPath)
            else {
                Log("Local JSON load failed, failed to parse JSON file \(self.filename).json")
                completion(nil)
                return
            }
            
            guard let dict = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments) as? [String : AnyObject]
            else {
                Log("Local JSON load failed, failed to parse JSON file \(self.filename).json")
                completion(nil)
                return
            }
            
            completion(dict)
        }
    }
}