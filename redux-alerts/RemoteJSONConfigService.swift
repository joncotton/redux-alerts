//
//  RemoteConfigJSONService.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Alamofire
import Mappable

// TODO: No injected network lib dependency here, wouldn't be like this in prod
struct RemoteJSONConfigService: ConfigService {
    let url: NSURL
    
    init(url: NSURL) {
        self.url = url
    }
    
    func latestAppConfig(completion: (AppConfig?) -> ()) {
        Alamofire.request(.GET, url).responseJSON { response in
            var config: AppConfig?
            if let JSON = response.result.value {
                do {
                    try config <- JSON as? MappingDataSource
                } catch {
                    Log("Remote config load failed")
                    completion(nil)
                }
            }
            
            completion(config)
        }
    }
}