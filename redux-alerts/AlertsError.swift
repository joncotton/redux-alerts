//
//  AlertError.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

enum AlertsError: ErrorType {
    case alertsUpdateFailed
}