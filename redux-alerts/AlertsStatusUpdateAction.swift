//
//  AlertsStatusUpdateAction.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

enum AlertsStatusUpdateAction: ActionProducingAction {
    typealias GenericStateType = AppState
    
    case requestUpdate(AlertsService)
    case updateFinishedWithAlerts([InAppAlert])
    case updateFinishedWithError(AlertsError)
    
    func execute(state: AppState) -> AppState {
        var mutableState = state
        
        switch self {
        case .requestUpdate(_):
            mutableState.inAppAlerts.updateState = .updating

        case .updateFinishedWithAlerts(let alerts):
            mutableState.inAppAlerts.alerts = alerts
            mutableState.inAppAlerts.updateState = .notUpdating
            
        case .updateFinishedWithError(let error):
            mutableState.inAppAlerts.previousUpdateError = error
            mutableState.inAppAlerts.updateState = .notUpdating

        }
        
        return mutableState
    }
    
    func produceAction(state: AppState, completion: (Action?) -> ()) {
        switch self {
        case .requestUpdate(let alertsService):
            if state.inAppAlerts.updateState != .updating {
                alertsService.latestAlerts { alerts in
                    var action: AlertsStatusUpdateAction
                    if let alerts = alerts {
                        action = .updateFinishedWithAlerts(alerts)
                    } else {
                        action = .updateFinishedWithError(.alertsUpdateFailed)
                    }
                    
                    completion(action)
                }
            }
            
         case .updateFinishedWithAlerts(_), .updateFinishedWithError(_):
            break
        }
    }
}