//
//  AppState.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct AppState {
    var config = ConfigState()
    var microfilterDiagnostics = MicrofilterDiagnosticsState()
    var inAppAlerts = AlertsState()
}