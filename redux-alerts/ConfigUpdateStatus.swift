//
//  ConfigUpdateStatus.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

enum ConfigUpdateStatus {
    case initialState
    case localConfigHasLoaded
    case remoteConfigHasLoaded
}