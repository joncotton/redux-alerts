//
//  RemoteJSONAlertsService.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct RemoteJSONAlertsService: AlertsService {
    let dataProvider: RemoteJSONDataProvider
    
    init(dataProvider: RemoteJSONDataProvider) {
        self.dataProvider = dataProvider
    }
    
    func latestAlerts(completion: ([InAppAlert]?) -> ()) {
        dataProvider.loadData { data in
            guard let data = data
            else {
                Log("Failed to load alerts")
                completion(nil)
                return
            }
            
            var alerts: [InAppAlert]?
            do {
                try alerts <- data["alerts"] as? [MappingDataSource]
            } catch {
                Log("Failed to map alerts")
                completion(nil)
                return
            }
            
            completion(alerts)
        }
    }
}