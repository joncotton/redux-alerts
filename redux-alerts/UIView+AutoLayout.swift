//
//  UIView+AutoLayout.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 15/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

extension UIView {
    
    func addSubviewAndPinToEdges(subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
        
        let views = ["subview": subview]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[subview]|", options: .AlignAllLeading, metrics: nil, views: views)
        let vConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[subview]|", options: .AlignAllLeading, metrics: nil, views: views)
        
        addConstraints(hConstraints)
        addConstraints(vConstraints)
    }
    
}