//
//  AppConfig.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct AppConfig: Mappable {
    var endPoints = EndPoints()
    
    mutating func mapFromDataSource(dataSource: MappingDataSource) throws {
        try endPoints <- dataSource["endPoints"] as? MappingDataSource
    }
}