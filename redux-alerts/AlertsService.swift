//
//  AlertsStore.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 15/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

protocol AlertsService {
    func latestAlerts(completion: ([InAppAlert]?) -> ())
}