//
//  UpdateMicrofilterFaultState.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

struct UpdateMicrofilterFaultStatusAction: Action {
    typealias GenericStateType = AppState
    
    let isFaulty: Bool
    
    init(isFaulty: Bool) {
        self.isFaulty = isFaulty
    }
    
    func execute(state: AppState) -> AppState {
        var mutableState = state
        mutableState.microfilterDiagnostics.microfilterIsFaulty = isFaulty
        return mutableState
    }
}