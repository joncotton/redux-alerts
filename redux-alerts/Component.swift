//
//  Component.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 09/04/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation

protocol ComponentProperties {}

protocol Component {
    typealias PropsType: ComponentProperties
    
    var currentProperties: PropsType? {get}
    func render(properties: PropsType)
}

protocol ComponentContainer {
    typealias ComponentType: Component
    typealias PropsType = ComponentType.PropsType
    
    var component: ComponentType! {get}
    func propertiesForState(state: AppState) -> PropsType
}

extension StateObserver where Self: ComponentContainer, Self.PropsType == Self.ComponentType.PropsType {
    func stateDidUpdate(state: AppState) {
        let props = propertiesForState(state)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.component.render(props)
        }
    }
}