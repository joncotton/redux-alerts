//
//  LocalJSONConfigService.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import Mappable

struct LocalJSONConfigService: ConfigService {
    let filename: String
    
    init(filename: String) {
        self.filename = filename
    }
    
    func latestAppConfig(completion: (AppConfig?) -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let bundle = NSBundle.mainBundle()
            guard let fullPath = bundle.pathForResource(self.filename, ofType: "json")
                else {
                    Log("Local config load failed, failed to find \(self.filename).json")
                    completion(nil)
                    return
            }
            
            guard let jsonData = NSData(contentsOfFile: fullPath)
                else {
                    Log("Local config load failed, failed to parse JSON file \(self.filename).json")
                    completion(nil)
                    return
            }
            
            guard let dict = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments) as? [String : AnyObject]
                else {
                    Log("Local config load failed, failed to parse JSON file \(self.filename).json")
                    completion(nil)
                    return
            }
            
            var config: AppConfig?
            do {
                try config <- dict
            } catch {
                Log("Local config load failed, failed to map JSON to AppConfig")
                completion(nil)
                return
            }
            
            completion(config)
        }
    }
}