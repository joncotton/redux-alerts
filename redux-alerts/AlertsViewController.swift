//
//  ViewController.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 14/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import UIKit

class AlertsViewController: UIViewController, StoreConsumer, StateObserver, AppConfigConsumer, ComponentContainer {    
    @IBOutlet var component: AlertsComponent!

    private var alertsService: AlertsService!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let alertsServiceURL = appConfig.endPoints.serviceApp.URLByAppendingPathComponent("/alerts")
        let alertsDataProvider = RemoteJSONDataProvider(url: alertsServiceURL)
        alertsService = RemoteJSONAlertsService(dataProvider: alertsDataProvider)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        store.addStateObserver(self)

        store.dispatch(AlertsStatusUpdateAction.requestUpdate(alertsService))
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.removeStateObserver(self)
    }
    
    func propertiesForState(state: AppState) -> AlertsProperties {
        var alertsAreVisible: Bool
        var loadingIndicatorIsVisible: Bool
        
        switch state.inAppAlerts.updateState {
            case .notUpdating:
                alertsAreVisible = true
                loadingIndicatorIsVisible = false
                
            case .updating:
                alertsAreVisible = false
                loadingIndicatorIsVisible = true
        }
        
        let alertsProps = AlertsProperties(
            alerts: state.inAppAlerts.alerts,
            alertsAreVisible: alertsAreVisible,
            loadingIndicatorIsVisible: loadingIndicatorIsVisible
        )
        
        return alertsProps
    }

    @IBAction func alertsButtonWasTapped(sender: UIButton) {
        store.dispatch(AlertsStatusUpdateAction.requestUpdate(alertsService))
    }
}

