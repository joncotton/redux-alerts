//
//  AlertsStatusUpdateActionTests.swift
//  redux-alerts
//
//  Created by Cotton, Jonathan (Mobile Developer) on 16/03/2016.
//  Copyright © 2016 Jon Cotton. All rights reserved.
//

import Foundation
import XCTest
@testable import redux_alerts

class AlertsStatusUpdateActionTests: XCTestCase {
    private static let testAlerts = [
        InAppAlert(title: "Alert One", description: "First alert description", imageName: "alert-1", actionURL: NSURL(string: "https://www.sky.com/")!)
    ]
    
    private class MockAlertsService: AlertsService {
        var shouldFail = false
        
        func latestAlerts(completion: ([InAppAlert]?) -> ()) {
            if shouldFail {
                completion(nil)
            } else {
                completion(AlertsStatusUpdateActionTests.testAlerts)
            }
        }
    }
    
    private var testAlerts: [InAppAlert] {
        return AlertsStatusUpdateActionTests.testAlerts
    }
    
    func testAlertsStateIsUpdatedCorrectlyWhenRequestingAnAlertsUpdate() {
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .notUpdating
        
        let action = AlertsStatusUpdateAction.requestUpdate(MockAlertsService())
        let newState = action.execute(currentState)
        
        XCTAssertEqual(newState.inAppAlerts.updateState, AlertsUpdateStatus.updating)
    }
    
    func testAlertsStateIsNotUpdatedWhenMakingMultipleUpdateRequestsWhilstAlreadyUpdating() {
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .updating
        
        let action = AlertsStatusUpdateAction.requestUpdate(MockAlertsService())
        let newState = action.execute(currentState)
        
        XCTAssertEqual(newState.inAppAlerts.updateState, AlertsUpdateStatus.updating)
    }
    
    func testAlertsStateIsUpdatedCorrectlyWhenAlertsUpdateFinishesSuccessfully() {
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .updating
        
        let action = AlertsStatusUpdateAction.updateFinishedWithAlerts(testAlerts)
        let newState = action.execute(currentState)
        
        XCTAssertEqual(newState.inAppAlerts.updateState, AlertsUpdateStatus.notUpdating)
        
        XCTAssertEqual(newState.inAppAlerts.alerts.count, testAlerts.count)

        let firstAlert = newState.inAppAlerts.alerts[0]
        XCTAssertEqual(firstAlert.title, testAlerts[0].title)
        XCTAssertEqual(firstAlert.description, testAlerts[0].description)
        XCTAssertEqual(firstAlert.imageName, testAlerts[0].imageName)
        XCTAssertEqual(firstAlert.actionURL.absoluteString, testAlerts[0].actionURL.absoluteString)
    }
    
    func testAlertsStateIsUpdatedCorrectlyWhenAlertsUpdateFinishesWithError() {
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .updating
        
        let action = AlertsStatusUpdateAction.updateFinishedWithError(AlertsError.alertsUpdateFailed)
        let newState = action.execute(currentState)
        
        XCTAssertEqual(newState.inAppAlerts.updateState, AlertsUpdateStatus.notUpdating)
        XCTAssertEqual(newState.inAppAlerts.previousUpdateError, AlertsError.alertsUpdateFailed)
    }
    
    func testAlertsSuceededActionIsProducedWhenReceivingAlertsFromAlertService() {
        let actionProducerCallbackCalledWithCorrectAction = expectationWithDescription("Correct action was produced")
        let alertsService = MockAlertsService()
        alertsService.shouldFail = false
        
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .notUpdating
        
        let action = AlertsStatusUpdateAction.requestUpdate(alertsService)
        action.produceAction(currentState) { action in
            if let alertsAction = action as? AlertsStatusUpdateAction {
                if case let AlertsStatusUpdateAction.updateFinishedWithAlerts(alerts) = alertsAction {
                    XCTAssertEqual(alerts.count, self.testAlerts.count)
                    
                    let firstAlert = alerts[0]
                    XCTAssertEqual(firstAlert.title, self.testAlerts[0].title)
                    XCTAssertEqual(firstAlert.description, self.testAlerts[0].description)
                    XCTAssertEqual(firstAlert.imageName, self.testAlerts[0].imageName)
                    XCTAssertEqual(firstAlert.actionURL.absoluteString, self.testAlerts[0].actionURL.absoluteString)
                    
                    actionProducerCallbackCalledWithCorrectAction.fulfill()
                } else {
                    XCTFail()
                }
            }
        }
        
        waitForExpectationsWithTimeout(1.0, handler: nil)
    }
    
    func testAlertsFailedActionIsProducedWhenReceivingNoAlertsFromAlertService() {
        let actionProducerCallbackCalledWithCorrectAction = expectationWithDescription("Correct action was produced")
        let alertsService = MockAlertsService()
        alertsService.shouldFail = true
        
        var currentState = AppState()
        currentState.inAppAlerts.updateState = .notUpdating
        
        let action = AlertsStatusUpdateAction.requestUpdate(alertsService)
        action.produceAction(currentState) { action in
            if let alertsAction = action as? AlertsStatusUpdateAction {
                if case let AlertsStatusUpdateAction.updateFinishedWithError(error) = alertsAction {
                    XCTAssertEqual(error, AlertsError.alertsUpdateFailed)
                    
                    actionProducerCallbackCalledWithCorrectAction.fulfill()
                } else {
                    XCTFail()
                }
            }
        }
        
        waitForExpectationsWithTimeout(1.0, handler: nil)
    }
    
}
