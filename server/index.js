var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/alerts', function(req, res) {
  setTimeout(function(){
    res.json({
      "alerts": [
        {
          "title": "An alert",
          "description": "Some alert description",
          "image-name": "alert-1",
          "action-url": "https://www.google.com"
        },
        {
          "title": "Another alert",
          "description": "Some alert description",
          "image-name": "alert-2",
          "action-url": "https://www.sky.com"
        },
        {
          "title": "Another alert",
          "description": "Some alert description",
          "image-name": "alert-2",
          "action-url": "https://www.sky.com"
        },
        {
          "title": "Another alert",
          "description": "Some alert description",
          "image-name": "alert-2",
          "action-url": "https://www.sky.com"
        },
        {
          "title": "Another alert",
          "description": "Some alert description",
          "image-name": "alert-2",
          "action-url": "https://www.sky.com"
        },
        {
          "title": "Another alert",
          "description": "Some alert description",
          "image-name": "alert-2",
          "action-url": "https://www.sky.com"
        }
      ]
    });
  }, 4000);
});

app.get('/config', function(req, res) {
  res.json({
    "endPoints": {
      "serviceApp": "http://localhost:3000"
    }
  });
});

app.listen(3000, function () {
  console.log('app listening on port 3000!');
});
